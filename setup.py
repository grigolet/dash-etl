from setuptools import setup

setup(
   name='dash-etl',
   version='0.4',
   description='A library to extract, transform and load dashboard data for CERN',
   author='Maria Maxouti',
   author_email='maria.maxouti@cern.ch',
   packages=['dashetl'],
   install_requires=['influxdb>=5.3.0','python-dotenv>=0.14.0'],
   url="https://gitlab.cern.ch/industrial-controls/services/dash/dash-etl/",
   classifiers=[
      "Programming Language :: Python :: 3",
      "License :: CERN Copyright",
      "Operating System :: OS Independent",
   ],
   python_requires='>=3.6',
)
