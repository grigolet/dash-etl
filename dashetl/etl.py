import os
import time
import logging

from dotenv import load_dotenv
load_dotenv()

from influxdb import InfluxDBClient
from influxdb import DataFrameClient

from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.sql import SparkSession

import pandas
from cern.nxcals.pyquery.builders import *

sc = None
spark = None

def set_spark_session(sparkSession, sparkContext):
    global spark
    global sc
    spark = sparkSession
    sc = sparkContext

def create_spark_session(sparkAppName, yarnMasterMode = "yarn"):
    global spark
    global sc
    conf = SparkConf()
    conf.setMaster(yarnMasterMode) 
    conf.setAppName(sparkAppName)
    sc = SparkContext(conf=conf)
    spark = SparkSession(sc)

def timing(f):
    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = f(*args, **kwargs)
        time2 = time.time()
        logging.info('{:s} function took {:.3f} s'.format(f.__name__, (time2-time1)))
        return ret
    return wrap

@timing
def build_dataset(measurements, start_date, end_date):
    global spark
    # Building the query and load data into spark dataframe
    winccoaData_builder = DataQuery.builder(spark).byEntities().system('WINCCOA') \
                                   .startTime(start_date).endTime(end_date)
    entityAliasStage = None
    for measurement in measurements:
        entityAliasStage=winccoaData_builder.entity().keyValue('variable_name', measurement)
    if entityAliasStage == None:
        logging.warning("No entity specified, not performing extraction")
        return None
    return entityAliasStage.buildDataset()

@timing
def select_and_sort(dataset):
    return dataset.select("nxcals_entity_id", "timestamp", "value","variable_name").sort("timestamp")

@timing
def filter(measurement, spark_dataframe ,filter_lambda=None):
    p_dataframe = spark_dataframe.filter("variable_name=='" + measurement + "'").toPandas()
    p_dataframe.index = pandas.to_datetime(p_dataframe.pop('timestamp'),unit='ns')
    field_columns = ["value"]
    if filter_lambda is not None:   
        p_dataframe.rename(columns={"value": "raw_value"}, inplace=True)
        p_dataframe["value"]= filter_lambda(p_dataframe["raw_value"])
        field_columns.append("raw_value")
    return (p_dataframe, field_columns)

@timing
def extraction(measurements, start_date, end_date, filter_lambda=None, tag_lookup_lambda = None, measurement_lookup_lambda = ( lambda x: x )):
    winccoaDataset = build_dataset(measurements, start_date, end_date)

    logging.warn("Connecting to INFLUXDB {}@{}:{} DB : {}".format(os.environ.get("INFLUXDB_USERNAME"),os.environ.get("INFLUXDB_HOST"), os.environ.get("INFLUXDB_PORT"), os.environ.get("INFLUXDB_DATABASE")))
    client = DataFrameClient(host=os.environ.get("INFLUXDB_HOST"), port=int(os.environ.get("INFLUXDB_PORT")),
                                 username=os.environ.get("INFLUXDB_USERNAME"), 
                                 password=os.environ.get("INFLUXDB_PASSWORD"), 
                                 database=os.environ.get("INFLUXDB_DATABASE"), ssl=True, verify_ssl=False)
    winccoaData = select_and_sort(winccoaDataset)
    
    for measurement in measurements:
        
        filter_result=filter(measurement, winccoaData,filter_lambda)
        my_dataframe = filter_result[0]
        print(my_dataframe.dtypes)
        tags_dictionary = {}
        
        if tag_lookup_lambda is not None:   
            tags_dictionary.update(tag_lookup_lambda(measurement))
            
        logging.debug("Sending dataframe to influxDB {} {}".format(measurement, filter_result[0].shape))
        client.write_points(dataframe = my_dataframe, measurement = measurement_lookup_lambda(measurement) , 
                    tags = tags_dictionary, 
                    field_columns = filter_result[1],
                    protocol = "line",
                    time_precision = "n")
